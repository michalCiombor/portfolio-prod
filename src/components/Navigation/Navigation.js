import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import "./Navigation.css";
// import Container from "../Container/Container";

class Navigation extends Component {
  state = {
    inputValue: "",
    shownValue: ""
  };
  componentDidMount() {}

  handleChange = e => {
    this.setState({
      inputValue: e.target.value.toUpperCase()
    });
  };
  handleClick = () => {
    if (this.state.shownValue.length === 0) {
      if (this.state.inputValue.length > 0) {
        this.setState({
          shownValue: this.state.inputValue,
          inputValue: ""
        });
        document
          .querySelector(".underNavText")
          .classList.add("underNavText--active");
      } else alert("Please put your name first :)");
    } else alert("Sorry, you had one chance only ;)");
  };
  navActivate = () => {
    document.querySelector(".nav__list").classList.toggle("nav__list--active");
    document.querySelector(".nav").classList.toggle("nav--active");
  };

  render() {
    return (
      <Router>
        <nav className="nav">
          <div className="searchBar">
            <input
              type="text"
              className="searchBar__input"
              placeholder="put your name here!"
              value={this.state.inputValue}
              onChange={this.handleChange}
            />
            <button className="searchBar__btn btn" onClick={this.handleClick}>
              START
            </button>
          </div>
          <div className="hamburger" onClick={this.navActivate}>
            <span className="hamburgerLine" />
            <span className="hamburgerLine" />
            <span className="hamburgerLine" />
          </div>
          <ul className="nav__list">
            <li className="list__element">
              <NavLink
                // to={Container}
                className="list__item"
                activeClassName="list__item--active"
              >
                About
              </NavLink>
            </li>
            <li className="list__element">
              <NavLink
                className="list__item"
                activeClassName="list__item--active"
              >
                Portfolio
              </NavLink>
            </li>
            <li className="list__element">
              <NavLink
                className="list__item"
                activeClassName="list__item--active"
              >
                Skills
              </NavLink>
            </li>
            <li className="list__element">
              <NavLink
                className="list__item"
                activeClassName="list__item--active"
              >
                Contact
              </NavLink>
            </li>
          </ul>
        </nav>
        <div>
          {this.state.shownValue.length > 0 ? (
            <p className="underNavText">
              Cześć{" "}
              <span className="underNavText__name">
                {this.state.shownValue}
              </span>
              , witaj na mojej stronie!
            </p>
          ) : (
            <p className="underNavText" />
          )}
        </div>
        <Route />
      </Router>
    );
  }
}

export default Navigation;
