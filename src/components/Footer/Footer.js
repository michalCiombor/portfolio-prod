import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <>
      <footer>
        <div className="pageFooter">
          <p className="pageFooter__text">Made with love | 2019</p>
        </div>
      </footer>
    </>
  );
};

export default Footer;
