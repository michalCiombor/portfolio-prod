import React from "react";
import "./Container.css";

const Container = () => {
  return (
    <>
      <div className="container">
        <div className="container__text">
          <h1 className="container__text--large">Michał Ciombor</h1>
          <p className="container__text--small">Front-end developer</p>
        </div>
      </div>
    </>
  );
};

export default Container;
