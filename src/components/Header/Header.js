import React from "react";
import "./Header.css";

const Header = () => {
  return (
    <>
      <div className="header">
        <div className="header__info">
          <h1 className="header__text--large">Michał</h1>
          <h3 className="header__text--small">
            P.S Pewnie widziałeś już lepsze :D
          </h3>
        </div>
        <div className="header__picture" />
      </div>
    </>
  );
};

export default Header;
