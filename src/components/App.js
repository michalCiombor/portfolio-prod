import React from "react";

import Navigation from "./Navigation/Navigation.js";
import Header from "./Header/Header.js";
import Container from "./Container/Container.js";
import Footer from "./Footer/Footer.js";
import "./App.css";

function App() {
  return (
    <div className="page">
      <header>
        <Header />
      </header>
      <section>
        <aside>
          <Navigation />
        </aside>

        <Container />
      </section>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default App;
